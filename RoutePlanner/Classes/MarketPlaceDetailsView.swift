//
//  MarketPlaceDetailsView.swift
//  RoutePlanner
//
//  Created by Nikhil on 29/11/20.
//

import UIKit

class MarketPlaceDetailsView: UIViewController,MTSlideToOpenDelegate  {

    lazy var customizeSlideToOpen: MTSlideToOpenView = {
        let slide = MTSlideToOpenView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 60))
            slide.sliderViewTopDistance = 0
            slide.thumbnailViewTopDistance = 4;
            slide.thumbnailViewStartingDistance = 4;
            slide.sliderCornerRadius = 28
            slide.thumnailImageView.backgroundColor = .white
            slide.draggedView.backgroundColor = .clear
            slide.delegate = self
            slide.thumnailImageView.image = #imageLiteral(resourceName: "hamburgerMenu").imageFlippedForRightToLeftLayoutDirection()
        slide.sliderBackgroundColor = #colorLiteral(red: 0.9529411765, green: 0.5411764706, blue: 0.2509803922, alpha: 1)
            return slide
        }()
    
    
    @IBOutlet weak var Slide_btn_View: UIView!
    @IBOutlet weak var Decline_btn: UIButton!
    @IBOutlet weak var address_Txt: UILabel!
    @IBOutlet weak var earinng_txt: UILabel!
    @IBOutlet weak var duration_Txt: UILabel!
    @IBOutlet weak var close_btn: UIButton!
    @IBOutlet weak var date_header_Txt: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.Slide_btn_View.addSubview(customizeSlideToOpen)
        
        // Do any additional setup after loading the view.
    }

    @IBAction func close_btn_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func Decline_btn_Action(_ sender: Any)
    {
        
    }
    
// MARK: MTSlideToOpenDelegate
    func mtSlideToOpenDelegateDidFinish(_ sender: MTSlideToOpenView) {
           let alertController = UIAlertController(title: "", message: "Done!", preferredStyle: .alert)
           let doneAction = UIAlertAction(title: "Okay", style: .default) { (action) in
               sender.resetStateWithAnimation(false)
           }
           alertController.addAction(doneAction)
           self.present(alertController, animated: true, completion: nil)
           
       }
}
