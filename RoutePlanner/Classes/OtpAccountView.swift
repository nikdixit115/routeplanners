//
//  OtpAccountView.swift
//  RoutePlanner
//
//  Created by Nikhil on 28/11/20.
//

import UIKit

class OtpAccountView: UIViewController {

    @IBOutlet weak var mobile_txt: UITextField!
    @IBOutlet weak var mobile_view: UIView!
    @IBOutlet weak var Continue_btn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.mobile_view.layer.cornerRadius = 5.0
        self.mobile_view.layer.borderWidth = 1.0
        self.mobile_view.layer.borderColor = UIColor.lightGray.cgColor
        self.mobile_view.layer.masksToBounds = true
        
        // Do any additional setup after loading the view.
    }

    //MARK:- Continue Button Action Method
    @IBAction func continue_btn_action(_ sender: Any)
    {
        
    }
    
}
