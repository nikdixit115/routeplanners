//
//  LeftDrawerView.swift
//  RoutePlanner
//
//  Created by Nikhil on 29/11/20.
//

import UIKit

class LeftDrawerView: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    var info_Arr = ["MarketPlace","Completed Routes","Earnings","Help & Support","Settings","Privacy Policy","Terms & Conditions","Logout"]
    @IBOutlet weak var hamburger_table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let nib = UINib.init(nibName: "DrawerFirstCell", bundle: nil)
        self.hamburger_table.register(nib, forCellReuseIdentifier: "DrawerFirstCell")
        
        let nib1 = UINib.init(nibName: "DrawerSecondCell", bundle: nil)
        self.hamburger_table.register(nib1, forCellReuseIdentifier: "DrawerSecondCell")
        
        let nib2 = UINib.init(nibName: "DrawerThirdCell", bundle: nil)
        self.hamburger_table.register(nib2, forCellReuseIdentifier: "DrawerThirdCell")
        
        let nib3 = UINib.init(nibName: "DrawerForthCell", bundle: nil)
        self.hamburger_table.register(nib3, forCellReuseIdentifier: "DrawerForthCell")
        
        
        // Do any additional setup after loading the view.
    }
    
    //MARK:- TableView Delegate & DataSources
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 3
        {
            return info_Arr.count
        }
        else
        {
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DrawerFirstCell", for: indexPath) as! DrawerFirstCell
            
            return cell
        }
        else if indexPath.section == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DrawerSecondCell", for: indexPath) as! DrawerSecondCell
            
            return cell
        }
        else if indexPath.section == 2
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DrawerThirdCell", for: indexPath) as! DrawerThirdCell
            cell.assigned_height.constant = CGFloat.init(44)*4.0
            cell.assigne_table.reloadData()
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DrawerForthCell", for: indexPath) as! DrawerForthCell
            cell.info_txt.textColor = UIColor.black
            cell.info_txt.text = info_Arr[indexPath.row]
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
            return UITableView.automaticDimension
        }
        else if indexPath.section == 3
        {
            return 44
        }
        else if indexPath.section == 2
        {
            return UITableView.automaticDimension
        }
        else
        {
            return  80
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.section == 1
        {
            UserDefaults.standard.setValue("switch", forKey: "drawer_id")
            if let drawerController = navigationController?.parent as? KYDrawerController {
                drawerController.setDrawerState(.closed, animated: true)
            }
        }
        else if indexPath.section == 3
        {
            if indexPath.row == 0
            {
                UserDefaults.standard.setValue("market", forKey: "drawer_id")
                if let drawerController = navigationController?.parent as? KYDrawerController {
                    drawerController.setDrawerState(.closed, animated: true)
                }
            }
            else if indexPath.row == 1
            {
                UserDefaults.standard.setValue("completedroute", forKey: "drawer_id")
                if let drawerController = navigationController?.parent as? KYDrawerController {
                    drawerController.setDrawerState(.closed, animated: true)
                }
                
            }
            else if indexPath.row == 2
            {
                UserDefaults.standard.setValue("Earn", forKey: "drawer_id")
                if let drawerController = navigationController?.parent as? KYDrawerController {
                    drawerController.setDrawerState(.closed, animated: true)
                }
                
            }
            else if indexPath.row == 4
            {
                UserDefaults.standard.setValue("setting", forKey: "drawer_id")
                if let drawerController = navigationController?.parent as? KYDrawerController {
                    drawerController.setDrawerState(.closed, animated: true)
                }
            }
           
        }
        else if indexPath.section == 0
        {
            UserDefaults.standard.setValue("profile", forKey: "drawer_id")
            if let drawerController = navigationController?.parent as? KYDrawerController {
                drawerController.setDrawerState(.closed, animated: true)
            }
        }
        
   
    }
    
}
