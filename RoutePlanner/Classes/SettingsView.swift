//
//  SettingsView.swift
//  RoutePlanner
//
//  Created by Nikhil on 29/11/20.
//

import UIKit

class SettingsView: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var back_btn: UIButton!
    @IBOutlet weak var profile_table: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let nib = UINib.init(nibName: "SettingFirstCell", bundle: nil)
        self.profile_table.register(nib, forCellReuseIdentifier: "SettingFirstCell")
        
        let nib1 = UINib.init(nibName: "SettingSecondCell", bundle: nil)
        self.profile_table.register(nib1, forCellReuseIdentifier: "SettingSecondCell")
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func back_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let  cell = tableView.dequeueReusableCell(withIdentifier: "SettingFirstCell", for: indexPath) as! SettingFirstCell
            cell.header_txt.text = "Vehicle"
            cell.vechile_subheading_Txt.text = "Vehicle Type"
            cell.drop_Txt.text = "Car"
            cell.custom_view.layer.cornerRadius = 5.0
            cell.custom_view.layer.borderColor = UIColor.lightGray.cgColor
            cell.custom_view.layer.borderWidth = 1.0
            cell.custom_view.layer.masksToBounds = true
            return cell
        }
        else if indexPath.section == 1
        {
            let  cell = tableView.dequeueReusableCell(withIdentifier: "SettingFirstCell", for: indexPath) as! SettingFirstCell
            cell.header_txt.text = "Units"
            cell.vechile_subheading_Txt.text = "Distance Unit"
            cell.drop_Txt.text = "Miles"
            cell.custom_view.layer.cornerRadius = 5.0
            cell.custom_view.layer.borderColor = UIColor.lightGray.cgColor
            cell.custom_view.layer.borderWidth = 1.0
            cell.custom_view.layer.masksToBounds = true
            return cell
        }
        else
        {
            let  cell = tableView.dequeueReusableCell(withIdentifier: "SettingSecondCell", for: indexPath) as! SettingSecondCell
            
            cell.custom_view.layer.cornerRadius = 5.0
            cell.custom_view.layer.borderColor = UIColor.lightGray.cgColor
            cell.custom_view.layer.borderWidth = 1.0
            cell.custom_view.layer.masksToBounds = true
            cell.custom_view1.layer.cornerRadius = 5.0
            cell.custom_view1.layer.borderColor = UIColor.lightGray.cgColor
            cell.custom_view1.layer.borderWidth = 1.0
            cell.custom_view1.layer.masksToBounds = true
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 2
        {
            return 231
        }
        else
        {
            return 136
        }
       
    }
    
}
