//
//  EarningsView.swift
//  RoutePlanner
//
//  Created by Nikhil on 29/11/20.
//

import UIKit

class EarningsView: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var Market_table_view: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        let nib = UINib.init(nibName: "MarkertHeaderView", bundle: nil)
        self.Market_table_view.register(nib, forHeaderFooterViewReuseIdentifier: "MarkertHeaderView")
        
        let nib1 = UINib.init(nibName: "MarketFirstCell", bundle: nil)
        self.Market_table_view.register(nib1, forCellReuseIdentifier: "MarketFirstCell")
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func back_btn_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MarketFirstCell", for: indexPath) as! MarketFirstCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerview = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.size.width, height: 52))
        
        headerview.backgroundColor = UIColor.darkGray
        let label = UILabel.init(frame: CGRect.init(x: 16, y: 12, width: 150, height: 21))
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 14.0)
        label.text = "Sunday 21-01-2021"
        label.textAlignment = .left
        headerview.addSubview(label)
        
//        let label1 = UILabel.init(frame: CGRect.init(x: tableView.frame.size.width-160, y: 12, width: 150, height: 21))
//        label1.textColor = UIColor.white
//        label1.text = "4 Offers"
//        label1.font = UIFont.boldSystemFont(ofSize: 14.0)
//        label1.textAlignment = .right
//        headerview.addSubview(label1)
        
        return headerview
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 44
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
       
    }

}
