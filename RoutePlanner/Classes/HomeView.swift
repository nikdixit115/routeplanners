//
//  HomeView.swift
//  RoutePlanner
//
//  Created by Nikhil on 29/11/20.
//

import UIKit
import MapKit

class HomeView: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    
    @IBOutlet weak var add_route_btn: UIButton!
    @IBOutlet weak var add_route_view: UIView!
    @IBOutlet weak var map_view: MKMapView!
    @IBOutlet weak var trans_ing: UIImageView!
    @IBOutlet weak var route_view: UIView!
    @IBOutlet weak var route_complete_btn: UIButton!
    @IBOutlet weak var info_complete_lbl: UILabel!
    var index_value = 0
    @IBOutlet weak var hamburger_btn: UIButton!
    @IBOutlet weak var Home_tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib.init(nibName: "HomeFirstCell", bundle: nil)
        self.Home_tableView.register(nib, forCellReuseIdentifier: "HomeFirstCell")
        
        let nib1 = UINib.init(nibName: "HomeAddressCell", bundle: nil)
        self.Home_tableView.register(nib1, forCellReuseIdentifier: "HomeAddressCell")
        
        let nib2 = UINib.init(nibName: "HomeStopCell", bundle: nil)
        self.Home_tableView.register(nib2, forCellReuseIdentifier: "HomeStopCell")
        
        self.Home_tableView.rowHeight = 143
        
        self.add_route_view.layer.cornerRadius = 5.0
        self.add_route_view.layer.masksToBounds = true
        
        self.add_route_view.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    @IBAction func Add_route_btn_action(_ sender: Any)
    {
        let route = self.storyboard?.instantiateViewController(withIdentifier: "AddRouteView") as! AddRouteView
        route.modalPresentationStyle = .overFullScreen
        self.present(route, animated: true, completion: nil)
        
    }
    override func viewWillAppear(_ animated: Bool)
    {
        let drawer_id = UserDefaults.standard.value(forKey: "drawer_id") as? String ?? ""
        
        if drawer_id == "switch"
        {
            UserDefaults.standard.removeObject(forKey: "drawer_id")
            let switchs = self.storyboard?.instantiateViewController(withIdentifier: "SwitchAccountView") as! SwitchAccountView
            switchs.modalPresentationStyle = .overFullScreen
            self.present(switchs, animated: true, completion: nil)
        }
        else if drawer_id == "market"
        {
            UserDefaults.standard.removeObject(forKey: "drawer_id")
            let switchs = self.storyboard?.instantiateViewController(withIdentifier: "MarketPlaceView") as! MarketPlaceView
            switchs.modalPresentationStyle = .overFullScreen
            self.present(switchs, animated: true, completion: nil)
            
        }
        else if drawer_id == "profile"
        {
            UserDefaults.standard.removeObject(forKey: "drawer_id")
            let switchs = self.storyboard?.instantiateViewController(withIdentifier: "ProfileEditView") as! ProfileEditView
            switchs.modalPresentationStyle = .overFullScreen
            self.present(switchs, animated: true, completion: nil)
        }
        else if drawer_id == "setting"
        {
            UserDefaults.standard.removeObject(forKey: "drawer_id")
            let switchs = self.storyboard?.instantiateViewController(withIdentifier: "SettingsView") as! SettingsView
            switchs.modalPresentationStyle = .overFullScreen
            self.present(switchs, animated: true, completion: nil)
        }
        else if drawer_id == "completedroute"
        {
            UserDefaults.standard.removeObject(forKey: "drawer_id")
            let switchs = self.storyboard?.instantiateViewController(withIdentifier: "CompletedRoutesView") as! CompletedRoutesView
            switchs.modalPresentationStyle = .overFullScreen
            self.present(switchs, animated: true, completion: nil)
            
        }
        else if drawer_id == "Earn"
        {
            UserDefaults.standard.removeObject(forKey: "drawer_id")
            let switchs = self.storyboard?.instantiateViewController(withIdentifier: "CompletedRoutesView") as! CompletedRoutesView
            switchs.modalPresentationStyle = .overFullScreen
            self.present(switchs, animated: true, completion: nil)
            
        }
        else
        {
            UserDefaults.standard.removeObject(forKey: "drawer_id")
        }
    }
    
    @IBAction func hamburger_btn_action(_ sender: Any)
    {
        if let drawerController = navigationController?.parent as? KYDrawerController {
            drawerController.setDrawerState(.opened, animated: true)
        }
    }
    
    @IBAction func route_complete_action(_ sender: Any)
    {
        self.route_view.isHidden = true
        self.trans_ing.isHidden = true
    }
    //MARK:- TableView Delegate & DataSources
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 2
        {
            return 1
        }
        else
        {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeFirstCell", for: indexPath) as! HomeFirstCell
            cell.Optimize_Route_Btn.addTarget(self, action: #selector(optimize_btn_action), for: .touchUpInside)
            return cell
        }
        else if indexPath.section == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeAddressCell", for: indexPath) as! HomeAddressCell
            cell.icon_img.image = #imageLiteral(resourceName: "homeicon")
            cell.address_lbl.text = "Start Address"
            return cell
        }
        else if indexPath.section == 2
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeStopCell", for: indexPath) as! HomeStopCell
            
//            if indexPath.row == self.index_value
//            {
//                cell.Start_btn.isHidden = false
//                cell.Delivered_btn.isHidden = false
//                cell.Failed_btn.isHidden = false
//            }
//            else
//            {
//                cell.Start_btn.isHidden = true
//                cell.Delivered_btn.isHidden = true
//                cell.Failed_btn.isHidden = true
//            }
            
            cell.Start_btn.layer.cornerRadius = 5.0
            cell.Delivered_btn.layer.cornerRadius = 5.0
            cell.Failed_btn.layer.cornerRadius = 5.0
            cell.contact_btn.layer.cornerRadius = 5.0
            cell.pod_btn.layer.cornerRadius = 5.0
            cell.Delivered_btn.addTarget(self, action: #selector(Delivered_btn_action), for: .touchUpInside)
            cell.Failed_btn.addTarget(self, action: #selector(Failed_btn_action), for: .touchUpInside)
            cell.Start_btn.addTarget(self, action: #selector(Start_btn_Action), for: .touchUpInside)
            return cell
            
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeAddressCell", for: indexPath) as! HomeAddressCell
            cell.icon_img.image = #imageLiteral(resourceName: "endicon")
            cell.address_lbl.text = "End Address"
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
//        if indexPath.section == 2
//        {
////            if indexPath.row == self.index_value
////            {
////                return UITableView.automaticDimension
////            }
////            else
////            {
////                return 100
////            }
//
//        }
//        else
//        {
//            return UITableView.automaticDimension
//        }
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
//        if indexPath.section == 2
//        {
//            self.index_value = indexPath.row
//            let indexPath = IndexPath(item: indexPath.row, section: 2)
//            self.Home_tableView.reloadRows(at: [indexPath], with: .none)
//        }
    }
    
    @objc func Delivered_btn_action()
    {
       // self.route_view.isHidden = false
       // self.trans_ing.isHidden = false
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DeliveredView") as! DeliveredView
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func Failed_btn_action()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FailedView") as! FailedView
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    @objc func Start_btn_Action()
    {
        self.route_view.isHidden = false
        self.trans_ing.isHidden = false
    }
    
    
    @objc func optimize_btn_action()
    {
        DispatchQueue.main.asyncAfter(deadline: .now()+0.4, execute:
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotAddedView") as! NotAddedView
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: true, completion: nil)
                                    
        })
    }
    
}
