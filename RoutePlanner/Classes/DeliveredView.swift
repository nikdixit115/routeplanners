//
//  DeliveredView.swift
//  RoutePlanner
//
//  Created by Nikhil on 29/11/20.
//

import UIKit

class DeliveredView: UIViewController {

    @IBOutlet weak var txt_tx: UITextView!
    @IBOutlet weak var note_dispatcher_txt: UITextView!
    @IBOutlet weak var close_btn: UIButton!
    @IBOutlet weak var submit_btn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.txt_tx.layer.cornerRadius = 5.0
        self.txt_tx.layer.borderWidth = 1.0
        self.txt_tx.layer.borderColor = UIColor.lightGray.cgColor
        
        
        self.note_dispatcher_txt.layer.cornerRadius = 5.0
        self.note_dispatcher_txt.layer.borderWidth = 1.0
        self.note_dispatcher_txt.layer.borderColor = UIColor.lightGray.cgColor
        // Do any additional setup after loading the view.
    }

    
    @IBAction func submit_btn_action(_ sender: Any)
    {
        let delivery = self.storyboard?.instantiateViewController(withIdentifier: "DeliveryProofView") as! DeliveryProofView
        delivery.modalPresentationStyle = .overFullScreen
        self.present(delivery, animated: true, completion: nil)
    }
    
    @IBAction func close_btn_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
}
