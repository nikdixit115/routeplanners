//
//  SwitchAccountView.swift
//  RoutePlanner
//
//  Created by Nikhil on 29/11/20.
//

import UIKit

class SwitchAccountView: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    
    @IBOutlet weak var continue_btn: UIButton!
    var info = ["Admin 1","Admin 2","Individual"]
    @IBOutlet weak var switch_account_table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        let nib = UINib.init(nibName: "SwitchFirstCell", bundle: nil)
        self.switch_account_table.register(nib, forCellReuseIdentifier: "SwitchFirstCell")
        
        let nib1 = UINib.init(nibName: "SwitchSecondCell", bundle: nil)
        self.switch_account_table.register(nib1, forCellReuseIdentifier: "SwitchSecondCell")
        
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func continue_btn_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 3
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 2
        {
            return info.count
        }
        else
        {
            return 1
        }
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SwitchFirstCell", for: indexPath) as! SwitchFirstCell
            
            return cell
        }
        else if indexPath.section == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SwitchSecondCell", for: indexPath) as! SwitchSecondCell
            cell.info_txt.text = "Switch Accounts"
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SwitchSecondCell", for: indexPath) as! SwitchSecondCell
            cell.info_txt.text = info[indexPath.row]
            
            if indexPath.row == 0
            {
                cell.info_txt.textColor = UIColor.init(hexString: ColorCodes().AppColor)
            }
            else
            {
                cell.info_txt.textColor = UIColor.black
            }
            
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
            return 265
        }
        else
        {
            return UITableView.automaticDimension
        }
        
    }
    
}
