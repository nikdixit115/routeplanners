//
//  NotAddedView.swift
//  RoutePlanner
//
//  Created by Nikhil on 29/11/20.
//

import UIKit

class NotAddedView: UIViewController {

    @IBOutlet weak var driver_btn: UIButton!
    @IBOutlet weak var retry_btn: UIButton!
    @IBOutlet weak var custom_view: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.custom_view.layer.cornerRadius = 5.0
        self.custom_view.layer.masksToBounds = true
        
        // Do any additional setup after loading the view.
    }

    @IBAction func retry_btn_action(_ sender: Any)
    {
        
    }
    
    @IBAction func driver_btn_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
}
