//
//  RegisterAccount.swift
//  RoutePlanner
//
//  Created by Nikhil on 28/11/20.
//

import UIKit

class RegisterAccount: UIViewController {
    
    @IBOutlet weak var create_account_btn: UIButton!
    @IBOutlet weak var Continue_btn: UIButton!
    @IBOutlet weak var password_txt: UITextField!
    @IBOutlet weak var password_view: UIView!
    @IBOutlet weak var mobile_txt: UITextField!
    @IBOutlet weak var mobile_view: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mobile_view.layer.cornerRadius = 5.0
        self.mobile_view.layer.borderWidth = 1.0
        self.mobile_view.layer.borderColor = UIColor.lightGray.cgColor
        self.mobile_view.layer.masksToBounds = true
        
        self.password_view.layer.cornerRadius = 5.0
        self.password_view.layer.borderWidth = 1.0
        self.password_view.layer.borderColor = UIColor.lightGray.cgColor
        self.password_view.layer.masksToBounds = true
        
        
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Create Account Action Method
    @IBAction func create_account_btn_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Continue Button Action Method
    @IBAction func continue_btn_action(_ sender: Any)
    {
        DispatchQueue.main.asyncAfter(deadline: .now()+0.4, execute:
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OtpAccountView") as! OtpAccountView
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: true, completion: nil)
                                    
        })
        
    }
    
}
