//
//  LoginView.swift
//  RoutePlanner
//
//  Created by Nikhil on 28/11/20.
//

import UIKit

class LoginView: UIViewController {

    @IBOutlet weak var create_account_btn: UIButton!
    @IBOutlet weak var Apple_signIn_btn: UIButton!
    @IBOutlet weak var SignInwithGoogle_btn: UIButton!
    @IBOutlet weak var Continue_btn: UIButton!
    @IBOutlet weak var password_txt: UITextField!
    @IBOutlet weak var password_view: UIView!
    @IBOutlet weak var mobile_txt: UITextField!
    @IBOutlet weak var mobile_view: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()


        self.mobile_view.layer.cornerRadius = 5.0
        self.mobile_view.layer.borderWidth = 1.0
        self.mobile_view.layer.borderColor = UIColor.lightGray.cgColor
        self.mobile_view.layer.masksToBounds = true
        
        self.password_view.layer.cornerRadius = 5.0
        self.password_view.layer.borderWidth = 1.0
        self.password_view.layer.borderColor = UIColor.lightGray.cgColor
        self.password_view.layer.masksToBounds = true
        
        self.Apple_signIn_btn.layer.cornerRadius = 5.0
        self.Continue_btn.layer.cornerRadius = 5.0
        self.SignInwithGoogle_btn.layer.borderColor = UIColor.lightGray.cgColor
        self.SignInwithGoogle_btn.layer.borderWidth = 1.0
        
        // Do any additional setup after loading the view.
    }
    //MARK:- Create Account Action Method
    @IBAction func create_account_btn_action(_ sender: Any)
    {
        DispatchQueue.main.asyncAfter(deadline: .now()+0.4, execute:
                                        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "RegisterAccount") as! RegisterAccount
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    //MARK:- Continue Button Action Method
    @IBAction func continue_btn_action(_ sender: Any)
    {
        DispatchQueue.main.asyncAfter(deadline: .now()+0.4, execute:
        {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "KYDrawerController") as! KYDrawerController
            let navigationController = UINavigationController(rootViewController: newViewController)
            navigationController.isNavigationBarHidden = true
            appdelegate.window!.rootViewController = navigationController
            
        
        })
       
        
    }
    
    //MARK:- Sign In Google Action Method
    @IBAction func SignInGoogle_action(_ sender: Any)
    {
        DispatchQueue.main.asyncAfter(deadline: .now()+0.4, execute:
                                        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PhoneAskView") as! PhoneAskView
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    // MARK:- Apple Sign In Action Method
    @IBAction func apple_signIn_action(_ sender: Any)
    {
        DispatchQueue.main.asyncAfter(deadline: .now()+0.4, execute:
                                        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PhoneAskView") as! PhoneAskView
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
}
