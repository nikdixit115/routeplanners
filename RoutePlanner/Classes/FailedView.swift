//
//  FailedView.swift
//  RoutePlanner
//
//  Created by Nikhil on 29/11/20.
//

import UIKit

class FailedView: UIViewController {

    @IBOutlet weak var submit_btn: UIButton!
    @IBOutlet weak var text_view: UITextView!
    @IBOutlet weak var close_btn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.text_view.layer.cornerRadius = 5.0
        self.text_view.layer.borderWidth = 1.0
        self.text_view.layer.borderColor = UIColor.lightGray.cgColor

        // Do any additional setup after loading the view.
    }

    @IBAction func close_btn_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func submit_btn_action(_ sender: Any)
    {
        
    }
}
