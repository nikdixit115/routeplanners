//
//  PhotoUploadView.swift
//  RoutePlanner
//
//  Created by Nikhil on 29/11/20.
//

import UIKit

class PhotoUploadView: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    

    @IBOutlet weak var table_phot_height: NSLayoutConstraint!
    @IBOutlet weak var submit_btn: UIButton!
    @IBOutlet weak var close_btn: UIButton!
    @IBOutlet weak var sign_view: UIView!
    @IBOutlet weak var upload_photo_bnt: UIButton!
    @IBOutlet weak var photo_upload_table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.sign_view.layer.cornerRadius = 5.0
        self.sign_view.layer.borderColor = UIColor.lightGray.cgColor
        self.sign_view.layer.borderWidth = 1.0
        
        let nib = UINib.init(nibName: "SwitchSecondCell", bundle: nil)
        self.photo_upload_table.register(nib, forCellReuseIdentifier: "SwitchSecondCell")
        
        self.table_phot_height.constant = 0.0
        self.photo_upload_table.isHidden = true
        // Do any additional setup after loading the view.
    }

    @IBAction func upload_photo_btn_Action(_ sender: Any)
    {
        
    }
    
    @IBAction func submit_btn_action(_ sender: Any)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "KYDrawerController") as! KYDrawerController
        let navigationController = UINavigationController(rootViewController: newViewController)
        navigationController.isNavigationBarHidden = true
        appdelegate.window!.rootViewController = navigationController
    }
    
    @IBAction func close_btn_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SwitchSecondCell", for: indexPath) as! SwitchSecondCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 44
    }
}
