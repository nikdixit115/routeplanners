//
//  MarketPlaceFilterView.swift
//  RoutePlanner
//
//  Created by Nikhil on 29/11/20.
//

import UIKit

class MarketPlaceFilterView: UIViewController,UITableViewDelegate,UITableViewDataSource
{
   
    
    var info_Arr = ["Location","Duration","Earnings"]
    @IBOutlet weak var filter_tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        let nib = UINib.init(nibName: "MarketFilterCell", bundle: nil)
        self.filter_tableView.register(nib, forCellReuseIdentifier: "MarketFilterCell")
        
        let nib1 = UINib.init(nibName: "DrawerForthCell", bundle: nil)
        self.filter_tableView.register(nib1, forCellReuseIdentifier: "DrawerForthCell")
        
    
        
        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 1
        {
            return info_Arr.count
        }
        else
        {
            return 1
        }
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MarketFilterCell", for: indexPath) as! MarketFilterCell
            cell.close_btn.addTarget(self, action: #selector(Close_btn_action), for: .touchUpInside)
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DrawerForthCell", for: indexPath) as! DrawerForthCell
            cell.info_txt.textColor = UIColor.black
            cell.info_txt.text = info_Arr[indexPath.row]
            return cell
        }
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 44
    }
    
    @objc func Close_btn_action()
    {
        self.dismiss(animated: true, completion: nil)
    }

}
