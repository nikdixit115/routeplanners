//
//  DeliveryProofView.swift
//  RoutePlanner
//
//  Created by Nikhil on 29/11/20.
//

import UIKit

class DeliveryProofView: UIViewController {

    @IBOutlet weak var submit_btn: UIButton!
    @IBOutlet weak var close_btn: UIButton!
    @IBOutlet weak var sign_view: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.sign_view.layer.cornerRadius = 5.0
        self.sign_view.layer.borderColor = UIColor.lightGray.cgColor
        self.sign_view.layer.borderWidth = 1.0
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func submit_btn_action(_ sender: Any)
    {
        let photo = self.storyboard?.instantiateViewController(withIdentifier: "PhotoUploadView") as! PhotoUploadView
        photo.modalPresentationStyle = .overFullScreen
        self.present(photo, animated: true, completion: nil)
    }
    
    @IBAction func close_btn_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
}

