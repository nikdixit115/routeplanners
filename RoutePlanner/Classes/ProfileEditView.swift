//
//  ProfileEditView.swift
//  RoutePlanner
//
//  Created by Nikhil on 29/11/20.
//

import UIKit

class ProfileEditView: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    

    @IBOutlet weak var back_btn: UIButton!
    @IBOutlet weak var profile_table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        let nib = UINib.init(nibName: "ProfileFirstCell", bundle: nil)
        self.profile_table.register(nib, forCellReuseIdentifier: "ProfileFirstCell")
        
        
        
        // Do any additional setup after loading the view.
    }
    

    @IBAction func back_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let  cell = tableView.dequeueReusableCell(withIdentifier: "ProfileFirstCell", for: indexPath) as! ProfileFirstCell
        cell.submit_btn.layer.cornerRadius = 5.0
        cell.full_name_view.layer.cornerRadius = 5.0
        cell.full_name_view.layer.borderWidth = 1.0
        cell.full_name_view.layer.borderColor = UIColor.lightGray.cgColor
        cell.mobile_number_view.layer.cornerRadius = 5.0
        cell.mobile_number_view.layer.borderWidth = 1.0
        cell.mobile_number_view.layer.borderColor = UIColor.lightGray.cgColor
        
        cell.change_password_btn.addTarget(self, action: #selector(ChangePasswordView_action), for: .touchUpInside)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 342
    }
    
    
    @objc func ChangePasswordView_action()
    {
        let chnage = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordView") as! ChangePasswordView
        chnage.modalPresentationStyle = .overFullScreen
        self.present(chnage, animated: true, completion: nil)
    }

}
