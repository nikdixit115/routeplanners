//
//  AddRouteView.swift
//  RoutePlanner
//
//  Created by Nikhil on 29/11/20.
//

import UIKit

class AddRouteView: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var Market_table_view: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib1 = UINib.init(nibName: "addrouteFirstCell", bundle: nil)
        self.Market_table_view.register(nib1, forCellReuseIdentifier: "addrouteFirstCell")
        
        let nib2 = UINib.init(nibName: "HomeAddressCell", bundle: nil)
        self.Market_table_view.register(nib2, forCellReuseIdentifier: "HomeAddressCell")
        
        let nib3 = UINib.init(nibName: "DrawerForthCell", bundle: nil)
        self.Market_table_view.register(nib3, forCellReuseIdentifier: "DrawerForthCell")
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func back_btn_action(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0
        {
            return 1
        }
        else if section == 1
        {
            return 2
        }
        else
        {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "addrouteFirstCell", for: indexPath) as! addrouteFirstCell
            
            cell.custom_view.layer.cornerRadius = 5.0
            cell.custom_view.layer.borderColor = UIColor.lightGray.cgColor
            cell.custom_view.layer.borderWidth = 1.0
            cell.custom_view.layer.masksToBounds = true
            return cell
        }
        else if indexPath.section == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeAddressCell", for: indexPath) as!  HomeAddressCell
            
            if indexPath.row == 0
            {
                cell.address_lbl.text = "Start Address"
                cell.icon_img.image = #imageLiteral(resourceName: "homeicon")
            }
            else
            {
                cell.address_lbl.text = "End Address"
                cell.icon_img.image = #imageLiteral(resourceName: "endicon")
            }
            return cell
           
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DrawerForthCell", for: indexPath) as!  DrawerForthCell
            cell.info_txt.text = "Stops"
            return cell
        }
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 54
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
       
    }

}
