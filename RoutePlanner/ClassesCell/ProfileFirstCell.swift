//
//  ProfileFirstCell.swift
//  RoutePlanner
//
//  Created by Nikhil on 29/11/20.
//

import UIKit

class ProfileFirstCell: UITableViewCell {

    @IBOutlet weak var change_password_btn: UIButton!
    @IBOutlet weak var submit_btn: UIButton!
    @IBOutlet weak var number_mobile_txt: UITextField!
    @IBOutlet weak var mobile_number_view: UIView!
    @IBOutlet weak var full_name_view: UIView!
    @IBOutlet weak var full_name_txt: UITextField!
    @IBOutlet weak var profile_btn: UIButton!
    @IBOutlet weak var profile_img: UIImageView!
    @IBOutlet weak var profile_view: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
