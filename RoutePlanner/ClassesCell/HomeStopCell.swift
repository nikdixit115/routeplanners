//
//  HomeStopCell.swift
//  RoutePlanner
//
//  Created by Nikhil on 29/11/20.
//

import UIKit

class HomeStopCell: UITableViewCell {

    @IBOutlet weak var pod_btn: UIButton!
    @IBOutlet weak var contact_btn: UIButton!
    @IBOutlet weak var Failed_btn: UIButton!
    @IBOutlet weak var Delivered_btn: UIButton!
    @IBOutlet weak var Start_btn: UIButton!
    @IBOutlet weak var sotp_lbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
