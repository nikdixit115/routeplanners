//
//  DrawerFirstCell.swift
//  RoutePlanner
//
//  Created by Nikhil on 29/11/20.
//

import UIKit

class DrawerFirstCell: UITableViewCell {

    @IBOutlet weak var edit_profile_btn: UIButton!
    @IBOutlet weak var driver_address: UILabel!
    @IBOutlet weak var driver_name_lbl: UILabel!
    @IBOutlet weak var driver_img: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
