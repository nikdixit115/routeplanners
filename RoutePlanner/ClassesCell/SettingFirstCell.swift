//
//  SettingFirstCell.swift
//  RoutePlanner
//
//  Created by Nikhil on 29/11/20.
//

import UIKit

class SettingFirstCell: UITableViewCell {

    @IBOutlet weak var custom_view: UIView!
    @IBOutlet weak var drop_Txt: UILabel!
    @IBOutlet weak var dropdown_btn: UIButton!
    @IBOutlet weak var vechile_subheading_Txt: UILabel!
    @IBOutlet weak var header_txt: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
