//
//  DrawerThirdCell.swift
//  RoutePlanner
//
//  Created by Nikhil on 29/11/20.
//

import UIKit

class DrawerThirdCell: UITableViewCell,UITableViewDelegate,UITableViewDataSource {
   
    

    @IBOutlet weak var assigned_height: NSLayoutConstraint!
    @IBOutlet weak var assigne_table: UITableView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let nib = UINib.init(nibName: "DrawerForthCell", bundle: nil)
        self.assigne_table.register(nib, forCellReuseIdentifier: "DrawerForthCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DrawerForthCell", for: indexPath) as! DrawerForthCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 44
    }
    
}
