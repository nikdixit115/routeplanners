//
//  HomeFirstCell.swift
//  RoutePlanner
//
//  Created by Nikhil on 29/11/20.
//

import UIKit

class HomeFirstCell: UITableViewCell {

    @IBOutlet weak var heading_btn: UILabel!
    @IBOutlet weak var SubHeading_btn: UILabel!
    @IBOutlet weak var Optimize_Route_Btn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.Optimize_Route_Btn.layer.cornerRadius = 5.0
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
